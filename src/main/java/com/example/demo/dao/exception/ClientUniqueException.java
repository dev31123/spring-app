package com.example.demo.dao.exception;

import com.example.demo.domain.Client;

public class ClientUniqueException extends RuntimeException {
    private final Client client;

    public ClientUniqueException(Client client) {
        this.client = client;
    }

    public ClientUniqueException(String message, Client client) {
        super(message);
        this.client = client;
    }

    public ClientUniqueException(String message, Throwable cause, Client client) {
        super(message, cause);
        this.client = client;
    }

    public ClientUniqueException(Throwable cause, Client client) {
        super(cause);
        this.client = client;
    }

    public ClientUniqueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Client client) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.client = client;
    }

    public Client getClient() {
        return client;
    }
}
