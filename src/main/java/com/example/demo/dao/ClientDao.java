package com.example.demo.dao;

import com.example.demo.domain.Client;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ClientDao {
    Long save(Client c);

    Client findOne(Long id);

    List<Client> findAll();

    boolean delete(Long id);
}
