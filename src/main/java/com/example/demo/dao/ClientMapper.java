package com.example.demo.dao;

import com.example.demo.domain.Client;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ClientMapper implements RowMapper<Client> {
    @Override
    public Client mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Client(
                resultSet.getLong("id"),
                resultSet.getString("full_name"),
                resultSet.getString("passport_number"),
                resultSet.getDate("birth_date").toLocalDate(),
                resultSet.getString("phone_number"),
                resultSet.getString("email")
        );
    }
}
