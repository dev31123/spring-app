package com.example.demo.dao;

import com.example.demo.dao.exception.ClientUniqueException;
import com.example.demo.dao.exception.StorageGeneralException;
import com.example.demo.domain.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class SqlClientDao implements ClientDao {
    private final static String INSERT_SQL = String.format(
            "INSERT INTO client (full_name, birth_date, passport_number, phone_number, email) " +
                    "VALUES (:%s, :%s, :%s, :%s, :%s)",
            ClientColumns.FULL_NAME, ClientColumns.BIRTH_DATE, ClientColumns.PASSPORT_NUMBER, ClientColumns.PHONE_NUMBER, ClientColumns.EMAIL);
    private final static String UPDATE_SQL = String.format("UPDATE client " +
                    "SET email = :%s, full_name = :%s, birth_date = :%s, passport_number = :%s, phone_number = :%s) " +
                    "WHERE id = :%s",
            ClientColumns.EMAIL,
            ClientColumns.FULL_NAME,
            ClientColumns.BIRTH_DATE,
            ClientColumns.PASSPORT_NUMBER,
            ClientColumns.PHONE_NUMBER,
            ClientColumns.ID
    );
    private final static String DELETE = String.format("DELETE FROM client WHERE id = %s", ClientColumns.ID);
    private final static String FIND_ONE = String.format("SELECT * FROM client WHERE id = :%s", ClientColumns.ID);
    private final static String FIND_ALL = String.format("SELECT * FROM client ORDER BY %s", ClientColumns.ID);

    private static final Logger logger = LoggerFactory.getLogger(SqlClientDao.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private ClientMapper clientMapper;

    public Client findOne(Long id) {
        Client client = null;

        try {
            client = jdbcTemplate.queryForObject(
                    FIND_ONE,
                    Map.of(ClientColumns.ID.toString(), id),
                    clientMapper
            );
        } catch (DataAccessException e) {
            logger.error(e.getMessage(), e);
            throw new StorageGeneralException(e.getMessage(), e);
        }

        return client;
    }

    public List<Client> findAll() {
        List<Client> result = null;

        try {
            result = jdbcTemplate.query(FIND_ALL, clientMapper);
        } catch (DataAccessException e) {
            logger.error(e.getMessage(), e);
            throw new StorageGeneralException(e.getMessage(), e);
        }

        return result;
    }

    public Long save(Client client) {
        KeyHolder kh = new GeneratedKeyHolder();

        if (client.getId() == null) {
            try {
                jdbcTemplate.update(
                        INSERT_SQL,
                        clientToInsertParameterSource(client),
                        kh, new String[]{ClientColumns.ID.toString()}
                );
            } catch (DuplicateKeyException e) {
                logger.error(e.getMessage(), e);
                throw new ClientUniqueException("A customer with this data is already registered", e, client);
            } catch (DataAccessException e) {
                logger.error(e.getMessage(), e);
                throw new StorageGeneralException(e.getMessage(), e);
            }

            return getIdFromKh(kh);
        } else {
            try {
                jdbcTemplate.update(UPDATE_SQL, clientToUpdateParameterSource(client));
            } catch (DataAccessException e) {
                logger.error(e.getMessage(), e);
                throw new StorageGeneralException(e.getMessage(), e);
            }

            return client.getId();
        }
    }

    public boolean delete(Long id) {
        boolean isDeleted = false;

        try {
            isDeleted = jdbcTemplate.update(DELETE, Map.of(ClientColumns.ID.toString(), id)) == 1;
        } catch (DataAccessException e) {
            logger.error(e.getMessage(), e);
            throw new StorageGeneralException(e.getMessage(), e);
        }

        return isDeleted;
    }

    private MapSqlParameterSource clientToInsertParameterSource(Client client) {
        return new MapSqlParameterSource()
                .addValue(ClientColumns.FULL_NAME.toString(), client.getFullName())
                .addValue(ClientColumns.BIRTH_DATE.toString(), client.getBirthDate())
                .addValue(ClientColumns.PASSPORT_NUMBER.toString(), client.getPassportNumber())
                .addValue(ClientColumns.PHONE_NUMBER.toString(), client.getPhoneNumber())
                .addValue(ClientColumns.EMAIL.toString(), client.getEmail());
    }

    private MapSqlParameterSource clientToUpdateParameterSource(Client client) {
        return new MapSqlParameterSource()
                .addValue(ClientColumns.ID.toString(), client.getId())
                .addValue(ClientColumns.FULL_NAME.toString(), client.getFullName())
                .addValue(ClientColumns.BIRTH_DATE.toString(), client.getBirthDate())
                .addValue(ClientColumns.PASSPORT_NUMBER.toString(), client.getPassportNumber())
                .addValue(ClientColumns.PHONE_NUMBER.toString(), client.getPhoneNumber())
                .addValue(ClientColumns.EMAIL.toString(), client.getEmail());
    }

    private Long getIdFromKh(KeyHolder kh) {
        Map<String, Object> keys = kh.getKeys();

        if (keys != null && keys.get(ClientColumns.ID.toString()) != null) {
            return (Long) keys.get(ClientColumns.ID.toString());
        } else {
            throw new StorageGeneralException("Can't get Primary Key of New Record");
        }
    }
}
