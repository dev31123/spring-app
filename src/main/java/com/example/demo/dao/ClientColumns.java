package com.example.demo.dao;

public enum ClientColumns {
    ID("id"),
    FULL_NAME("full_name"),
    BIRTH_DATE("birth_date"),
    PASSPORT_NUMBER("passport_number"),
    PHONE_NUMBER("phone_number"),
    EMAIL("email");

    private final String columnName;

    ClientColumns(String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String toString() {
        return columnName;
    }
}
