package com.example.demo.rest;

import com.example.demo.dao.exception.ClientUniqueException;
import com.example.demo.domain.Client;
import com.example.demo.domain.ClientService;
import com.example.demo.rest.dto.ClientDto;
import com.example.demo.rest.dto.ErrorResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ClientController {
    private final static Logger logger = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @PostMapping("/clients")
    public void postClient(@Valid @RequestBody ClientDto clientDto) {
        if (clientDto.id != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        clientService.registerClient(clientDto.toClient());
    }

    @GetMapping("/clients")
    public List<ClientDto> clients() {
        List<Client> clients = clientService.getClients();

        List<ClientDto> result = new ArrayList<>(clients.size());
        for (Client client : clients) {
            result.add(ClientDto.valueOf(client));
        }

        return result;
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponseDto runtimeExceptionHandler(ClientUniqueException e) {
        return new ErrorResponseDto("Internal server error", Instant.now(), e.getClient().getClass().getSimpleName());
    }

    @ExceptionHandler(value = ClientUniqueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDto clientUniqueExceptionHandler(ClientUniqueException e) {
        return new ErrorResponseDto(e.getMessage(), Instant.now(), e.getClient().getClass().getSimpleName());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDto argumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        String s = e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(", "));
        return new ErrorResponseDto(s, Instant.now(), e.getTarget().getClass().getSimpleName());
    }
}
