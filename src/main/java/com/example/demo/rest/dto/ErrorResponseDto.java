package com.example.demo.rest.dto;

import java.time.Instant;

public class ErrorResponseDto {
    public final String msg;
    public final Instant time;
    public final String className;

    public ErrorResponseDto(String msg, Instant time, String className) {
        this.msg = msg;
        this.time = time;
        this.className = className;
    }
}
