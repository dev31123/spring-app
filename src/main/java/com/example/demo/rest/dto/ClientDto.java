package com.example.demo.rest.dto;

import com.example.demo.domain.Client;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class ClientDto {
    public final Long id;

    @Size(min = 2, max = 255, message = "fullName length must be more than 1 and less than 256")
    public final String fullName;

    @Size(min = 10, max = 10, message = "length must be equals 10")
    @Pattern(regexp = "[0-9]*", message = "fullName must be only symbols")
    public final String passportNumber;

    public final LocalDate birthDate;

    @Size(min = 11, max = 11, message = "phoneNumber must be only symbols")
    public final String phoneNumber;

    @Email(message = "email must be valid email")
    public final String email;

    public ClientDto(Long id, String fullName, String passportNumber, LocalDate birthDate, String phoneNumber, String email) {
        this.id = id;
        this.fullName = fullName;
        this.passportNumber = passportNumber;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    static public ClientDto valueOf(Client client) {
        return new ClientDto(client.getId(), client.getFullName(), client.getPassportNumber(), client.getBirthDate(), client.getPhoneNumber(), client.getEmail());
    }

    public Client toClient() {
        return new Client(id, fullName, passportNumber, birthDate, phoneNumber, email);
    }
}
