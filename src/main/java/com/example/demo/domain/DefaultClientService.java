package com.example.demo.domain;

import com.example.demo.dao.ClientDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultClientService implements ClientService {
    private final static Logger logger = LoggerFactory.getLogger(DefaultClientService.class);

    @Autowired
    ClientDao clientRepository;

    public Long registerClient(Client client) {
        return clientRepository.save(client);
    }

    public List<Client> getClients() {
        return clientRepository.findAll();
    }
}
