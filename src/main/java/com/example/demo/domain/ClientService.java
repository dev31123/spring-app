package com.example.demo.domain;

import java.util.List;

public interface ClientService {
    Long registerClient(Client client);

    List<Client> getClients();
}
