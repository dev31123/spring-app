package com.example.demo.domain;

import java.time.LocalDate;

public class Client {
    private Long id;
    private String fullName;
    private String passportNumber;
    private LocalDate birthDate;
    private String phoneNumber;
    private String email;

    public Client() {
    }

    public Client(Long id, String fullName, String passportNumber, LocalDate birthDate, String phoneNumber, String email) {
        this.id = id;
        this.fullName = fullName;
        this.passportNumber = passportNumber;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhone(String phone) {
        this.phoneNumber = phone;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
