package com.example.demo.dao;

import com.example.demo.domain.Client;
import liquibase.Liquibase;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SqlClientDaoTest {

    @Autowired
    SqlClientDao clientRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    ClientMapper clientMapper;

    @MockBean
    Liquibase liquibase;

    private final static Client client1 = new Client(
            null,
            "John Doe I",
            "1234567801",
            LocalDate.now(),
            "7999112201",
            "user1@example.com"
    );

    private final static Client client2 = new Client(
            null,
            "John Doe II",
            "1234567802",
            LocalDate.now(),
            "7999112202",
            "user2@example.com"
    );

    @Test
    @Order(1)
    @Transactional
    @Rollback
    void save() {
        Long id = clientRepository.save(client1);
        Client client = jdbcTemplate.queryForObject("SELECT * FROM client WHERE id = ?", clientMapper, id);

        assertNotNull(client, "Клиент не сохранен в БД");
        assertEquals(client1.getFullName(), client.getFullName(), "ФИО клиента сохранено не верно");
        assertEquals(client1.getPassportNumber(), client.getPassportNumber(), "Номер паспорта клиента сохранен не верно");
        assertEquals(client1.getBirthDate(), client.getBirthDate(), "Дата рождения клиента сохранено не верно");
        assertEquals(client1.getPhoneNumber(), client.getPhoneNumber(), "Телефонный номер клиента сохранен не верно");
        assertEquals(client1.getEmail(), client.getEmail(), "Email клиента сохранен неверно");
    }

    @Test
    @Order(2)
    @Transactional
    @Rollback
    void findOne() {
        Long clientId = clientRepository.save(client1);
        Client client = clientRepository.findOne(clientId);

        assertNotNull(client, "Сохраненный клиент не найден в БД");
        assertEquals(client1.getFullName(), client.getFullName(), "ФИО найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getPassportNumber(), client.getPassportNumber(), "Паспортный номер найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getBirthDate(), client.getBirthDate(), "Дата рождения найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getPhoneNumber(), client.getPhoneNumber(), "Телефонный номер найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getEmail(), client.getEmail(), "Email найденного клиента не совпадает с сохраняемым");
    }

    @Test
    @Order(3)
    @Transactional
    @Rollback
    void findAll() {
        clientRepository.save(client1);
        clientRepository.save(client2);

        List<Client> dbClients = clientRepository.findAll();

        assertEquals(dbClients.size(), 2, "Найдено неверное количество клиентов");

        Client dbClient1 = dbClients.get(0);
        assertEquals(client1.getFullName(), dbClient1.getFullName(), "ФИО найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getPassportNumber(), dbClient1.getPassportNumber(), "Паспортный номер найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getBirthDate(), dbClient1.getBirthDate(), "Дата рождения найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getPhoneNumber(), dbClient1.getPhoneNumber(), "Телефонный номер найденного клиента не совпадает с сохраняемым");
        assertEquals(client1.getEmail(), dbClient1.getEmail(), "Email найденного клиента не совпадает с сохраняемым");

        Client dbClient2 = dbClients.get(1);
        assertEquals(client2.getFullName(), dbClient2.getFullName(), "ФИО найденного клиента не совпадает с сохраняемым");
        assertEquals(client2.getPassportNumber(), dbClient2.getPassportNumber(), "Паспортный номер найденного клиента не совпадает с сохраняемым");
        assertEquals(client2.getBirthDate(), dbClient2.getBirthDate(), "Дата рождения найденного клиента не совпадает с сохраняемым");
        assertEquals(client2.getPhoneNumber(), dbClient2.getPhoneNumber(), "Телефонный номер найденного клиента не совпадает с сохраняемым");
        assertEquals(client2.getEmail(), dbClient2.getEmail(), "Email найденного клиента не совпадает с сохраняемым");
    }

    @Test
    @Order(4)
    @Transactional
    @Rollback
    void delete() {
        Long clientId = clientRepository.save(client1);
        assertTrue(clientRepository.delete(clientId), "Метод удаления клиента вернул false вместо true");
        assertEquals(clientRepository.findAll().size(), 0, "Клиент не был удален");
    }
}
